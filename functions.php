<?php
require_once( 'bin/custom-meta-boxes/custom-meta-boxes.php' );
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_filter( 'rwmb_meta_boxes', array( $this, 'meta_boxes_offices' ) );
		add_filter( 'rwmb_meta_boxes', array( $this, 'meta_boxes_team' ) );
		add_filter( 'rwmb_meta_boxes', array( $this, 'meta_boxes_portfolio' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		register_nav_menus( array( 'main' ) );
		load_theme_textdomain('kalrock', get_template_directory() . '/languages');
		parent::__construct();
	}
	function register_post_types() {
		register_post_type( 'offices', array(
				'labels' => array(
					'name' => __( 'Offices' ),
					'singular_name' => __( 'Office' ),
					'add_new' => __( 'Add New Office', 'textdomain' ),
			        'add_new_item' => __( 'Add New Office', 'textdomain' ),
			        'new_item' => __( 'New Office', 'textdomain' ),
			        'edit_item' => __( 'Edit Office', 'textdomain' ),
			        'view_item' => __( 'View Office', 'textdomain' ),
			        'all_items' => __( 'All Offices', 'textdomain' ),
				),
				'public' => true,
				'has_archive' => true,
				'publicly_queryable'  => false,
				'show_in_rest' => true,
				'rewrite' => array('slug' => 'offices'),
				'supports'      => array('title')
			)
		);
		register_post_type( 'team', array(
				'labels' => array(
					'name' => __( 'Team' ),
					'singular_name' => __( 'Team' ),
					'add_new' => __( 'Add New Team Member', 'textdomain' ),
			        'add_new_item' => __( 'Add New Team Member', 'textdomain' ),
			        'new_item' => __( 'New Team Member', 'textdomain' ),
			        'edit_item' => __( 'Edit Team Member', 'textdomain' ),
			        'view_item' => __( 'View Team Member', 'textdomain' ),
			        'all_items' => __( 'All Team Members', 'textdomain' ),
				),
				'public' => true,
				'has_archive' => true,
				'publicly_queryable'  => true,
				'show_in_nav_menus' => true,
				'show_in_menu'        => TRUE,
				'show_in_nav_menus'   => TRUE,
				'show_in_rest' => true,
				'rewrite' => array('slug' => 'team'),
				'supports'      => array('title','excerpt', 'thumbnail', 'editor')
			)
		);
		register_post_type( 'portfolio', array(
				'labels' => array(
					'name' => __( 'Portfolio' ),
					'singular_name' => __( 'Portfolio' ),
					'add_new' => __( 'Add New Company', 'textdomain' ),
			        'add_new_item' => __( 'Add New Company', 'textdomain' ),
			        'new_item' => __( 'New Company', 'textdomain' ),
			        'edit_item' => __( 'Edit Company', 'textdomain' ),
			        'view_item' => __( 'View Companies', 'textdomain' ),
			        'all_items' => __( 'All Companies', 'textdomain' ),
				),
				'public' => true,
				'has_archive' => true,
				'publicly_queryable'  => true,
				'show_in_nav_menus' => true,
				'show_in_menu'        => TRUE,
				'show_in_nav_menus'   => TRUE,
				'show_in_rest' => true,
				'rewrite' => array('slug' => 'portfolio'),
				'supports'      => array('title', 'thumbnail')
			)
		);
	}
	function meta_boxes_offices( $meta_boxes ) {
	    $meta_boxes[] = array(
	        'title'      => __( 'Office', 'textdomain' ),
	        'post_types' => 'offices',
	        'fields'     => array(
	            array(
	                'id'   => 'street',
	                'name' => __( 'Street', 'textdomain' ),
	                'type' => 'text',
	            ),
	            array(
	                'id'   => 'region',
	                'name' => __( 'Region', 'textdomain' ),
	                'type' => 'text',
	            ),
	            array(
	                'id'   => 'country',
	                'name' => __( 'Country', 'textdomain' ),
	                'type' => 'text',
	            ),
	            array(
	                'id'   => 'phone',
	                'name' => __( 'Phone', 'textdomain' ),
	                'type' => 'text',
	            ),
	            array(
	                'id'   => 'order',
	                'name' => __( 'Order', 'textdomain' ),
	                'type' => 'text',
	            ),
	        ),
	    );
	    return $meta_boxes;
	}
	function meta_boxes_team( $meta_boxes ) {
	    $meta_boxes[] = array(
	        'title'      => __( 'Socials (URL)', 'textdomain' ),
	        'post_types' => 'team',
	        'fields'     => array(
	            array(
	                'id'   => 'linkedin',
	                'name' => __( 'LinkedIn', 'textdomain' ),
	                'type' => 'url',
	            ),
	            array(
	                'id'   => 'angel',
	                'name' => __( 'Angel.co', 'textdomain' ),
	                'type' => 'url',
	            ),
	            array(
	                'id'   => 'twitter',
	                'name' => __( 'Twitter', 'textdomain' ),
	                'type' => 'url',
	            ),
	        ),
	    );
	    return $meta_boxes;
	}
	function meta_boxes_portfolio( $meta_boxes ) {
	    $meta_boxes[] = array(
	        'title'      => __( 'Company URL', 'textdomain' ),
	        'post_types' => 'portfolio',
	        'fields'     => array(
	            array(
	                'id'   => 'url',
	                'type' => 'url',
	            )
	        ),
	    );
	    return $meta_boxes;
	}
	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		$context['getLocale'] = get_locale();
		$context['allLanguages'] = pll_the_languages(array('raw'=>1));
		$context['localeFound'] = load_theme_textdomain('kalrock', get_template_directory() . '/languages');
		return $context;
	}


	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		return $twig;
	}

}

new StarterSite();

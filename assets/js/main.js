window.$ = window.jQuery = require('jquery');
let slick = require('slick-carousel-browserify');
const maps = require('./maps.js');
// Hamburger icon
document.getElementById('hamburger').addEventListener('click', checkNav);
document.getElementsByClassName('menu')[0].addEventListener('click', closeNav);
window.addEventListener("keyup", function(e) {
    if (e.keyCode == 27) closeNav();
}, false);

function checkNav() {
    if (document.body.classList.contains('hamburger-active')) {
        closeNav();
    } else {
        openNav();
    }
}

function closeNav() {
    document.body.classList.remove('hamburger-active');
}

function openNav() {
    document.body.classList.add('hamburger-active');
}
// Member
function displayMemberContent(el) {
    var elParent = $(el).parent();
    elParent.hide();
    elParent.siblings(".member-about").fadeIn(300, 'swing');
}

function hideMemberContent(el) {
    var elParent = $(el).parent();
    elParent.hide();
    elParent.siblings(".content-wrapper").fadeIn(300, 'swing');
}
$(".about-member-more").on('click', function(event) {
    event.preventDefault();
    displayMemberContent(event.target);
});
$(".about-member-less").on('click', function(event) {
    event.preventDefault();
    hideMemberContent(event.target);
});
$('.slider').slick({
    infinite: true,
    arrows: true,
    dots: false,
    nextArrow: '<div class="slider-arrow arrow-left"></div>',
    prevArrow: '<div class="slider-arrow arrow-right"></div>',
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
            breakpoint: 559,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
});

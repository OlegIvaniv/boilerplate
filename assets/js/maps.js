let $ = require('jquery');
let initMap = function() {
    var elements = document.getElementsByClassName('office-item');
    var location = null;
    for (var i = 0; i < elements.length; i++) {
        var address = $(elements[i]).find("#office-street").text() + ' ' + $(elements[i]).find("#office-region").text()
        $.ajax({
          url: "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDDxSpko9l134t7_oUOZnKe0GzVe1MAN8M&address=" + address,
          cache: false,
          async: false,
          success: function (data) {
              location = data.results[0].geometry.location;
          }
        })
        var mapEl = $(elements[i]).find("#map")[0];
        var styledMapType = new google.maps.StyledMapType(
            [{ "featureType": "administrative", "elementType": "all", "stylers": [{ "saturation": "-100" }] }, { "featureType": "administrative.province", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": "50" }, { "visibility": "simplified" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": "-100" }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "all", "stylers": [{ "lightness": "30" }] }, { "featureType": "road.local", "elementType": "all", "stylers": [{ "lightness": "40" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "hue": "#ffff00" }, { "lightness": -25 }, { "saturation": -97 }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "lightness": -25 }, { "saturation": -100 }] }], { name: 'Styled Map' });

        // Create a map object, and include the MapTypeId to add
        // to the map type control.
        var map = new google.maps.Map(mapEl, {
            center: location,
            zoom: 16,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            panControl: true,
            streetViewControl: false,
            draggable: true,
            overviewMapControl: true,
            mapTypeControlOptions: {
                mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                    'styled_map'
                ]
            }
        });

        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');

        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }
}
window.initMap = initMap;
<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['pages'] = Timber::get_posts( array('post_type' => 'page','posts_per_page' => -1));
$context['offices'] = Timber::get_posts( array('post_type' => 'offices','posts_per_page' => -1, 'orderby'   => 'meta_value_num', 'meta_key'  => 'order'));
$context['team'] = Timber::get_posts( array('post_type' => 'team','posts_per_page' => -1));
$context['portfolio'] = Timber::get_posts( array('post_type' => 'portfolio','posts_per_page' => -1));
$templates = array( 'index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'home.twig' );
}
Timber::render( $templates, $context );

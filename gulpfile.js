const gulp = require('gulp'),
    // Styles
    sass = require('gulp-sass'),
    normalize = require('node-normalize-scss'),
    sourcemaps = require('gulp-sourcemaps'),
    // PostCSS
    postcss = require('gulp-postcss'),
    cssnano = require('cssnano'),
    autoprefixer = require('autoprefixer'),
    // General
    sizereport = require('gulp-sizereport');
    browserify = require('browserify'),
    browserSync = require('browser-sync'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream');
    // JS
    uglify = require('gulp-uglify');


/* pathConfig*/
const templatePath = '.'
    entryPoint = templatePath + '/assets/js/main.js',
    sassWatchPath = templatePath + '/assets/styles/**/*.scss',
    jsWatchPath = templatePath + '/assets/js/**/*.js',
    templatesWatchPath = templatePath + '/templates/*.twig';
/**/

gulp.task('browser-sync', () => {
    const config = {
        open: false,
        host: 'localhost',
        proxy: 'http://bewy.localhost:8000',
        port: 2222
    };

    return browserSync(config);
});
gulp.task('js', () => {
    return browserify(entryPoint, { debug: true, extensions: ['es6'] })
        .transform("babelify", { presets: ["es2015"] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write())
        .pipe(sizereport({
            gzip: true
        }))
        .pipe(gulp.dest(templatePath + '/build/js/'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('sass', () => {
    const processors = [
        autoprefixer
    ];
    return gulp.src(sassWatchPath)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: normalize.includePaths
        }).on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sourcemaps.write())
        .pipe(sizereport({
            gzip: true
        }))
        .pipe(gulp.dest(templatePath + '/build/css'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('sass-prod', () => {
    const processors = [
        autoprefixer,
        cssnano
    ];
    return gulp.src(sassWatchPath)
        .pipe(sass({
            includePaths: normalize.includePaths
        }).on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sizereport({
            gzip: true
        }))
        .pipe(gulp.dest(templatePath + '/build/css'))
});
gulp.task('js-prod', () => {
    return browserify(entryPoint, { debug: true, extensions: ['es6'] })
        .transform("babelify", { presets: ["es2015"] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest(templatePath + '/build/js/'))
        .pipe(sizereport({
            gzip: true
        }))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('watch', () => {
    gulp.watch(jsWatchPath, ['js']);
    gulp.watch(sassWatchPath, ['sass']);
    gulp.watch(templatesWatchPath, () => {
        return gulp.src('')
            .pipe(browserSync.reload({ stream: true }))
    });
});
gulp.task('prod', ['js-prod', 'sass-prod']);
gulp.task('default', ['js', 'sass', 'watch', 'browser-sync']);
